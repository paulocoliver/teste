# 
Teste DB1 
==============================

Requirements:
-------
- [PHP](http://www.php.net/) 7.0 or later.
- [Git](https://git-scm.com/downloads)
- [Composer](https://getcomposer.org/)


Installation:
-------
```bash
$ git clone https://gitlab.com/paulocoliver/teste.git teste-db1
$ cd teste-db1
$ composer install
```

Criar e configurar o banco de dados:

```bash
$ mysql -u root -p
$ CREATE DATABASE `db1_teste` DEFAULT CHARACTER SET = `utf8`;
$ exit
$ edit config/database.php
$ cd database/
$ ../vendor/bin/propel migrate
$ cd ..
```

Executar servidor embutido do PHP:
```bash
$ php -S localhost:8888 -t public
```

Access your browser: [http://localhost:8888](http://localhost:8888)