<?php
namespace App\Controllers;

use App\Models\User;

class Auth extends Base
{
    protected $required_authentication = false;

    public function login() {
         processRequest(function () {
            if ($this->isAuthenticated()) {
                $this->redirectTo('/');
            }

            if ($this->isPost()) {
                if (User::authentication($_POST)) {
                    $this->redirectTo('/');
                }
            }
        });

        return $this->view('auth.login');
    }

    public function register() {
        processRequest(function () {
            if ($this->isAuthenticated()) {
                $this->redirectTo('/');
            }

            if ($this->isPost()) {
                $user = User::create($_POST);
                if (!empty($user)) {
                    if (User::authentication($_POST, $user)) {
                        $this->redirectTo('/');
                    }
                }
            }
        });

        return $this->view('auth.register');
    }

    public function logout() {
        User::removeAuthentication();
        $this->redirectTo('/login');
    }
}