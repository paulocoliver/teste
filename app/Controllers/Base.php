<?php
namespace App\Controllers;

use App\Services\View;
use App\Models\User;

abstract class Base
{
    protected $required_authentication = true;

    /** @var User */
    protected $authenticated_user;

    public function __construct()
    {
        if ($this->required_authentication && !$this->isAuthenticated()) {
            $this->redirectTo('login');
        }
        $this->authenticated_user = User::getAuthenticated();
    }

    protected function isAuthenticated () {
        return User::isAuthenticated();
    }

    /**
     * @param $file
     * @param array $params
     * @return string
     */
    protected function view ($file, array $params=[], $layout=null) {
        $params['authentication'] = User::class;
        return View::resolve($file, $params, $layout);
    }

    protected function isPost() {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }

    protected function redirectTo($uri, $permanently=false) {
        if ($permanently) {
            header( "HTTP/1.1 301 Moved Permanently" );
        }
        header( "Location: $uri" );
        exit;
    }
}