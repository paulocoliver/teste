<?php
namespace App\Controllers;

class Errors extends Base
{
    protected $required_authentication = false;

    public function error404() {
        return $this->view('errors.404');
    }
}