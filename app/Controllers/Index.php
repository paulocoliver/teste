<?php
namespace App\Controllers;

use App\Exceptions\ValidationException;
use App\Models\MathOperation;

class Index extends Base
{
    public function calculator() {

        $mathOperation = null;
        processRequest(function () use (&$mathOperation) {
            if (!empty($_POST['expression'])) {
                try {
                    $result = \PauloColiver\Calculator\Calculator::calculate($_POST['expression']);
                    $mathOperation = MathOperation::create([
                        'user_id' => $this->authenticated_user->id,
                        'math_operation' => $_POST['expression'],
                        'result' => $result,
                    ]);
                } catch (\Exception $e) {
                    throw new ValidationException(['expression' => ['Expressão inválida: '.$_POST['expression']]]);
                }
            }
        });

        return $this->view('index.calculator', ['mathOperation' => $mathOperation]);
    }
}