<?php
namespace App\Exceptions;

class ValidationException extends \Exception
{
    public $errors = [];

    public function __construct(array $errors)
    {
        parent::__construct('Os dados fornecidos são inválidos.');
        $this->errors = $errors;
    }

    public function getErrors () {
        return $this->errors;
    }

    public function hasErrors () {
        return !empty($this->errors);
    }
}