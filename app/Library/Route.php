<?php
namespace App\Library;

class Route
{
    private $uri;
    private $action;

    public function __construct($uri, $action)
    {
        $this->uri = $uri;
        $this->action = $action;
    }

    public function getAction() {
        return $this->action;
    }

    public function getUri() {
        return $this->uri;
    }
}