<?php
namespace App\Models;

use App\Exceptions\ValidationException;
use App\Traits\Models\Builder;
use App\Traits\Models\Events;
use App\Traits\Validation;

abstract class Base
{
    use Events, Builder, Validation;

    protected $table = null;

    protected $primary_key = 'id';

    protected $attributes = [];

    protected $exists = false;

    protected $fillable = [];

    public function __construct(array $attributes=[])
    {
        $this->attributes = $attributes;

        if (empty($this->table)) {
            try {
                $this->table = strtolower(get_class_basename($this)).'s';
            } catch (\Exception $e) {
            }
        }

        static::bootValidation();
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setAttribute($key, $value) {
        $this->attributes[$key] = $value;
        return $this;
    }

    /**
     * @param $key
     * @param $default
     * @return mixed
     */
    public function getAttribute($key, $default=null) {
        return isset($this->attributes[$key]) ? $this->attributes[$key] : $default;
    }

    public function getPrimaryKeyName() {
        return $this->primary_key;
    }

    public function getPrimaryKeyValue() {
        return $this->getAttribute($this->getPrimaryKeyName());
    }

    /**
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    /**
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

    /**
     * @param  string  $key
     * @return bool
     */
    public function __isset($key)
    {
        return !is_null($this->getAttribute($key));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function fillable() {

        if (!is_array($this->attributes)) {
            throw new \Exception('Invalid type $attributes');
        }

        if ($this->fillable === false) {
            return $this->attributes;
        }

        if (is_array($this->fillable) && !empty($this->fillable)) {
            $values = [];
            foreach ($this->fillable as $fill) {
                $values[$fill] = $this->getAttribute($fill);
            }
            return $values;

        } else {
            throw new \Exception('Invalid type or empty $fillable');
        }
    }

    /**
     * @throws \Exception | ValidationException
     */
    public function save() {

        if (static::eventTrigger('saving', $this) === false) {
            return false;
        }

        if ($this->exists) {

            if (static::eventTrigger('updating', $this) === false) {
                return false;
            }

            $values = $this->fillable();
            unset($values[$this->getPrimaryKeyName()]);

            $sql = static::getBuilder()->update($this->table)
                ->where([$this->getPrimaryKeyName() => $this->getPrimaryKeyValue()])
                ->set($values);

            static::executeSql($sql);

            static::eventTrigger('updated', $this);

        } else {

            if (static::eventTrigger('creating', $this) === false) {
                return false;
            }

            $values = $this->fillable();
            unset($values[$this->getPrimaryKeyName()]);

            /** @var \mysqli_stmt $result */
            $result = static::executeSql(
                static::getBuilder()->insert($this->table)->values($values)
            )->getResource();

            if (empty($result->affected_rows))
                throw new \Exception('Error insert');

            $this->setAttribute($this->getPrimaryKeyName(), $result->insert_id);
            $this->exists = true;

            static::eventTrigger('created', $this);
        }

        static::eventTrigger('saved', $this);

        return true;
    }

    /**
     * @param array $attributes
     * @return bool|static
     * @throws \Exception | ValidationException
     */
    public static function create(array $attributes) {
        $instance = static::newInstance($attributes);
        return $instance->save() ? $instance : false;
    }


}