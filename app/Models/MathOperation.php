<?php
namespace App\Models;

/**
 * Class User
 *
 * @package App\Models
 * @property int $id
 * @property string $user_id
 * @property string $math_operation
 * @property string $result
 * @property string $created_at
 */
class MathOperation extends Base
{
    protected $table = 'math_operations';

    protected $fillable = ['user_id', 'math_operation', 'result', 'created_at'];

    public function __construct(array $attributes=[])
    {
        parent::__construct($attributes);

        static::onCreating(function (MathOperation $model) {
            $model->created_at = date('Y-m-d H:i:s');
        });

        static::onUpdating(function () {
            throw new \Exception('Não permitido alteração.');
        });
    }

}