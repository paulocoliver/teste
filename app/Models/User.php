<?php
namespace App\Models;

use App\Traits\Authentication;
use App\Traits\Validation;

/**
 * Class User
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $created_at
 * @property string $updated_at
 */
class User extends Base
{
    use Authentication;

    protected $fillable = ['name', 'email', 'password', 'created_at', 'updated_at'];

    public function __construct(array $attributes=[])
    {
        parent::__construct($attributes);

        static::onCreating(function (User $model) {
            if (empty($model->name))
                $model->name = $model->email;
        });

        static::onValidated(function (User $model) {
            if (strlen($model->password) < 30) {
                $model->password   = md5($model->password);
            }
            $model->updated_at = date('Y-m-d H:i:s');
            if (!$model->exists) {
                $model->created_at = $model->updated_at;
            }
        });
    }

    public function validate() {
        if (!$this->validateRequired('name')) {
            $this->addError('name', 'O Campo Nome é obrigatório');
        }

        if (!$this->validateRequired('email')) {
            $this->addError('email', 'O Campo Email é obrigatório');
        }

        if (!$this->validateEmail('email')) {
            $this->addError('email', 'O Campo Email é invalido');
        }

        if (!$this->validateUnique('email')) {
            $this->addError('email', "O e-mail {$this->email} já está em uso");
        }

        if (!$this->validateRequired('password')) {
            $this->addError('password', 'O Campo Senha é obrigatório');
        }

        if (strlen($this->password) < 6) {
            $this->addError('password', 'O Campo Senha precisa ter no mínimo 6 caracteres.');
        }
    }
}