<?php
namespace App\Services;

class Authentication
{
    private $name = '';

    private $session;

    public function __construct($name)
    {
        $this->name = $name;
        $this->session = &$_SESSION[$this->name];
    }


    public static function init($name) {
        if (empty($name)) {
            throw new \Exception('Session invalid name');
        }
        return new static($name);
    }

    public function create($model) {
        $this->session = $model;
    }

    public function isAuthenticated() {
        return !empty($this->session);
    }

    public function getAuthenticated() {
        return $this->session;
    }

    public function remove() {
        return $this->session = null;
    }

}