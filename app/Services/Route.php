<?php
namespace App\Services;

class Route
{
    private static $registers = [];

    /**
     * @param string $uri
     * @param \Closure|string $action
     * @throws \Exception
     * @return \App\Library\Route
     */
    public static function register ($uri, $action) {
        if (empty($uri))
            throw new \Exception('Invalid uri Routes');

        $route = new \App\Library\Route($uri, $action);
        static::$registers[$uri] = $route;

        return $route;
    }

    /**
     * @throws \Exception
     */
    public static function run () {
        session_start();

        $path = $_SERVER['PATH_INFO'] ?? '/';
        if (empty(static::$registers[$path])) {
            $controllerObject = new \App\Controllers\Errors();
            $action = 'error404';

        } else {
            /** @var \App\Library\Route $route */
            $route = static::$registers[$path];
            $action = $route->getAction();

            if ($action instanceof \Closure) {
                $result = $action();
            } else {

                list($controller, $action) = explode('@', $action);

                $controller = "\App\Controllers\\$controller";
                if (!class_exists($controller)) {
                    throw new \Exception("Controller not exists: $controller");
                }

                $controllerObject = new $controller;
                if (!method_exists($controllerObject, $action)) {
                    throw new \Exception("Action $action not exists in $controller");
                }

            }
        }

        $result = $controllerObject->$action();
        echo $result;
    }
}