<?php
namespace App\Services;

use App\Exceptions\ValidationException;

class View
{
    private $content = '';

    private $params = [];

    private $layout_file = '';

    private $view_file = '';

    /**
     * @var ValidationException
     */
    private static $validationException;

    private function __construct(array $params=[])
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return mixed
     */
    public function getParam($key, $default=null)
    {
        return $this->hasParam($key) ? $this->params[$key] : $default;
    }

    public function hasParam($key): bool
    {
        return array_key_exists($key, $this->params);
    }

    public function loadFile($file) {
        $file = str_replace('.', '/', $file);
        $file = APP_PATH_VIEWS."/$file.php";

        if (!file_exists($file)) {
            throw new \Exception("View file not found: $file");
        }
        require $file;
    }

    public function showView() {
        $this->loadFile($this->view_file);
    }

    /**
     * @return \App\Traits\Authentication
     */
    private function authentication() {
        $authentication = $this->getParam('authentication');
        return $authentication;
    }

    public function isAuthenticated() {
        return $this->authentication() && $this->authentication()::isAuthenticated();
    }

    /**
     * @param $file
     * @return static
     */
    public static function resolve($file, array $params=[], $layout=null) {
        $view = new static($params);
        $view->view_file = $file;

        if (is_null($layout))
            $layout = 'default';

        if (!empty($layout)) {
            $view->layout_file = 'layout.'.$layout;
            $file = $view->layout_file;
        }

        ob_start();
        $view->loadFile($file);
        $result = ob_get_contents();
        $view->content = $result;
        ob_end_clean();

        return $view;
    }

    public function getContent() {
        return $this->content;
    }

    public function helperUrl($name) {

    }

    public function __toString()
    {
        return $this->getContent();
    }

    /**
     * @return ValidationException
     */
    public static function getValidationException(): ValidationException
    {
        return static::$validationException;
    }

    /**
     * @param ValidationException $validationException
     */
    public static function setValidationException(ValidationException $validationException)
    {
        static::$validationException = $validationException;
    }

    public static function hasErros()
    {
        return !empty(static::$validationException) && static::$validationException->hasErrors();
    }

    public static function getErros()
    {
        return !empty(static::$validationException) ? static::$validationException->getErrors() : [];
    }

    public function includeMsgErrors($key=null)
    {
        $errorsMsgs = static::getErros();
        if (!empty($key)) {
            $errorsMsgs = !empty($errorsMsgs[$key]) ? $errorsMsgs[$key] : [];
        }
        $this->params['errorsMsgs'] = $errorsMsgs;
        $this->loadFile('errors.include-msg-errors');
    }
}