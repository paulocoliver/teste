<?php
namespace App\Traits;

use App\Exceptions\ValidationException;
use App\Services\Authentication as AuthenticationService;

/**
 * Trait Authentication
 *
 * @package App\Traits
 * @mixin \App\Models\Base
 */
trait Authentication
{
    protected static function authSessionName() {
        return strtolower(get_class_basename(static::class));
    }

    protected static function authenticationByCredentials($credentials) {
        $model = new static();
        $select = $model->newSelect();
        $select->where(['email' => $credentials['email']]);
        return $model::first($select);
    }

    /**
     * @return AuthenticationService
     */
    private static function initAuthenticationService() {
        return AuthenticationService::init(static::authSessionName());
    }

    public static function getAuthenticated() {
        return static::initAuthenticationService()->getAuthenticated();
    }

    public static function isAuthenticated() {
        return static::initAuthenticationService()->isAuthenticated();
    }

    public static function authentication($credentials, $model=null) {
        if (empty($model) || !$model instanceof static) {
            $model = static::authenticationByCredentials($credentials);
        }

        if (empty($model)) {
            throw new ValidationException(['email' => ['E-mail não encontrado']]);
        }

        if (empty($credentials['password']) || $model->password != md5($credentials['password'])) {
            throw new ValidationException(['password' => ['Senha invalida']]);
        }


        if (!empty($model)) {
            static::initAuthenticationService()->create($model);
            return true;
        }
        return false;
    }

    public static function removeAuthentication() {
        static::initAuthenticationService()->remove();
    }
}