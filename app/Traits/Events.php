<?php
namespace App\Traits;

trait Events
{
    protected static $observables = [];

    /**
     * @return string
     */
    public static function eventNamespace() {
        return static::class;
    }

    /**
     * @param $eventName
     * @return string
     * @throws \Exception
     */
    protected static function eventGetName($eventName) {
        if (empty($eventName) || !is_string($eventName))
            throw new \Exception('Invalid eventName');

        return $eventName.'.'.static::eventNamespace();
    }

    /**
     * @param $eventName
     * @param callable $callback
     * @param int $priority
     * @throws \Exception
     */
    public static function eventListen($eventName, callable $callback, int $priority=0) {
        static::$observables[static::eventGetName($eventName)][intval($priority)][] = $callback;
    }

    /**
     * @param $eventName
     * @param array $params
     * @return bool|null
     * @throws \Exception
     */
    protected static function eventTrigger($eventName, $params=[]) {

        if (empty(static::$observables[static::eventGetName($eventName)]) || !is_array(static::$observables[static::eventGetName($eventName)]))
            return null;

        krsort(static::$observables[static::eventGetName($eventName)]);

        foreach (static::$observables[static::eventGetName($eventName)] as $events) {
            foreach ($events as $callback) {
                $return = $callback($params);
                if ($return === false) {
                    return false;
                }
            }
        }
    }
}