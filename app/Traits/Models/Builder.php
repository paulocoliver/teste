<?php
namespace App\Traits\Models;

use Zend\Db\Adapter\Driver\Mysqli\Result;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

/**
 * Trait Builder
 *
 * @package App\Traits\Models
 * @mixin \App\Models\Base
 */
trait Builder
{
    public static function getBuilder() {
        if (empty(APP_CONFIG_DATABASE['connections']['db1_teste'])) {
            throw new \Exception('Database not configured');
        }
        return new Sql(new Adapter(APP_CONFIG_DATABASE['connections']['db1_teste']));
    }

    /**
     * @return static
     */
    public static function newInstance(array $attributes=[]) {
        return new static($attributes);
    }

    /**
     * @return Select
     */
    public static function newSelect() {
        $model = static::newInstance();
        return static::getBuilder()->select()->from($model->table);
    }


    /**
     * @param $sql
     * @return Result
     */
    public static function executeSql($sql) {
        $statement = static::getBuilder()->prepareStatementForSqlObject($sql);
        return $statement->execute();
    }

    /**
     * @param $select
     * @return ResultSet
     */
    public static function get(Select $select) {
        $result = static::executeSql($select);

        $resultSet = new ResultSet;
        $newResult = [];
        if ($result instanceof ResultInterface && $result->isQueryResult()) {

            foreach ($result as $row) {
                $newResult[] = static::newInstanceDb($row);
            }

            $resultSet->initialize($newResult);
        }

        return $resultSet;
    }

    private static function newInstanceDb($row) {
        $model = self::newInstance($row);
        $model->exists = true;
        return $model;
    }

    /**
     * @param $id
     * @return static | array
     */
    public static function find($id) {
        $model = new static();
        $select = $model->newSelect();
        $select->where([$model->primary_key => $id])
            ->limit(1);
        return static::first($select);
    }

    public static function first(Select $select) {
        $select->limit(1);
        $result = static::executeSql($select);
        $row = $result->current();
        return !empty($row) ? static::newInstanceDb($row) : null;
    }
}