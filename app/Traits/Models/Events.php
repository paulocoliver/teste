<?php
namespace App\Traits\Models;

trait Events
{
    use \App\Traits\Events;

    /**
     * Register a saving model event with the dispatcher.
     *
     * @param  \Closure|string $callback
     * @param int $priority
     * @return void
     */
    public static function onSaving($callback, int $priority=0)
    {
        static::eventListen('saving', $callback, $priority);
    }

    /**
     * Register a saved model event with the dispatcher.
     *
     * @param  \Closure|string $callback
     * @param int $priority
     * @return void
     */
    public static function onSaved($callback, int $priority=0)
    {
        static::eventListen('saved', $callback, $priority);
    }

    /**
     * Register an updating model event with the dispatcher.
     *
     * @param  \Closure|string $callback
     * @param int $priority
     * @return void
     */
    public static function onUpdating($callback, int $priority=0)
    {
        static::eventListen('updating', $callback, $priority);
    }

    /**
     * Register an updated model event with the dispatcher.
     *
     * @param  \Closure|string $callback
     * @param int $priority
     * @return void
     */
    public static function onUpdated($callback, int $priority=0)
    {
        static::eventListen('updated', $callback, $priority);
    }

    /**
     * Register a creating model event with the dispatcher.
     *
     * @param  \Closure|string $callback
     * @param int $priority
     * @return void
     */
    public static function onCreating($callback, int $priority=0)
    {
        static::eventListen('creating', $callback, $priority);
    }

    /**
     * Register a created model event with the dispatcher.
     *
     * @param  \Closure|string $callback
     * @param int $priority
     * @return void
     */
    public static function onCreated($callback, int $priority=0)
    {
        static::eventListen('created', $callback, $priority);
    }

    /**
     * Register a deleting model event with the dispatcher.
     *
     * @param  \Closure|string $callback
     * @param int $priority
     * @return void
     */
    public static function onDeleting($callback, int $priority=0)
    {
        static::eventListen('deleting', $callback, $priority);
    }

    /**
     * Register a deleted model event with the dispatcher.
     *
     * @param  \Closure|string $callback
     * @param int $priority
     * @return void
     */
    public static function onDeleted($callback, int $priority=0)
    {
        static::eventListen('deleted', $callback, $priority);
    }
}