<?php
namespace App\Traits;

use App\Exceptions\ValidationException;

/**
 * Trait Validation
 *
 * @package App\Traits
 * @mixin \App\Models\Base
 */
trait Validation
{
    protected $errors = [];

    protected static function validationEventsPriority(int $additional=0): int {
        return -100 + $additional;
    }

    protected static function bootValidation()
    {
        static::onCreating(function ($class) {
            /** @var static $class */
            return $class->executeValidate();
        }, static::validationEventsPriority());

        static::onUpdating(function ($class) {
            /** @var static $class */
            return $class->executeValidate();
        }, static::validationEventsPriority());
    }

    public function addError($key, $message)
    {
        $this->errors[$key][] = $message;
        return $this;
    }

    public function addErrors(array $messages)
    {
        foreach ($messages as $key => $message) {
            $this->addError($key, $message);
        }
        return $this;
    }

    /**
     * @return bool
     * @throws ValidationException
     */
    public function executeValidate()
    {
        static::eventTrigger('validating', $this);

        $this->validate();
        if (!empty($this->errors)) {
            throw new ValidationException($this->errors);
        }

        static::eventTrigger('validated', $this);

        return true;
    }

    public function validate()
    {

    }

    public function validateUnique($field) {
        $select = static::newSelect()
            ->where([$field => $this->getAttribute($field)]);

        if (!empty($this->getPrimaryKeyValue())) {
            $select->where->notEqualTo($this->getPrimaryKeyName(), $this->getPrimaryKeyValue());
        }
        return empty(static::first($select));
    }

    public function validateRequired($field) {
        return !empty($this->getAttribute($field));
    }

    public function validateEmail($field) {
        return filter_var($this->getAttribute($field), FILTER_VALIDATE_EMAIL) !== false;
    }

    /**
     * @param  \Closure|string $callback
     * @param int $priority
     * @return void
     */
    public static function onValidating($callback, int $priority=0)
    {
        static::eventListen('validating', $callback, $priority);
    }

    /**
     * @param  \Closure|string $callback
     * @param int $priority
     * @return void
     */
    public static function onValidated($callback, int $priority=0)
    {
        static::eventListen('validated', $callback, $priority);
    }

}