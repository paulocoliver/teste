<?php

if (!function_exists('get_class_basename')) {
    /**
     * @return string
     */
    function get_class_basename($class)
    {
        try {
            return (new \ReflectionClass($class))->getShortName();
        } catch (\Exception $e) {
            return null;
        }
    }
}

if (!function_exists('processRequest')) {
    function processRequest(\Closure $function_ok, \Closure $function_error = null)
    {
        try {
            return $function_ok();
        } catch (App\Exceptions\ValidationException $error) {
            \App\Services\View::setValidationException($error);
            return false;
        } catch (\Exception $error) {
            if (!empty($function_error)) {
                return $function_error($error);
            } else {
                echo $error->getMessage();
                exit;
            }

        }
    }
}