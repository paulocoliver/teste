<?php
return [
    'connections' => [
        'db1_teste' => [
            'driver'     => 'Mysqli',
            'host'       => '127.0.0.1',
            'user'       => 'root',
            'password'   => 'root',
            'database'   => 'db1_teste',
        ],
        'db1_teste_propel' => [
            'adapter'    => 'mysql',
            'classname'  => 'Propel\Runtime\Connection\ConnectionWrapper',
            'dsn'        => 'mysql:host=127.0.0.1;dbname=db1_teste',
            'user'       => 'root',
            'password'   => 'root',
        ]
    ],/* 21:24:43 LOCAL Golden Codes */

];