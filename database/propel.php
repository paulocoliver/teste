<?php
$database = require __DIR__.'/../config/database.php';

return [
    'propel' => [
        'database' => [
            'connections' => [
                'db1_teste' => $database['connections']['db1_teste_propel']
            ]
        ],
        'runtime' => [
            'defaultConnection' => 'db1_teste',
            'connections' => ['db1_teste']
        ],
        'generator' => [
            'defaultConnection' => 'db1_teste',
            'connections' => ['db1_teste']
        ]
    ]
];