<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('APP_PATH', realpath(__DIR__.'/..'));
define('APP_PATH_PUBLIC', realpath(__DIR__));
define('APP_PATH_VIEWS', APP_PATH.'/views/');
define('APP_PATH_CONFIG', APP_PATH.'/config/');
define('APP_CONFIG_DATABASE', (require_once APP_PATH_CONFIG.'database.php'));

require __DIR__.'/../vendor/autoload.php';

use \App\Services\Route;

try {

    Route::register('/', 'Index@calculator');

    Route::register('/login', 'Auth@login');
    Route::register('/register', 'Auth@register');
    Route::register('/logout', 'Auth@logout');
    Route::register('/report', 'Reports@mathOperations');

    Route::register('/teste', function () {
        echo '<pre>';
        print_r('ok');
        echo '</pre>';
        exit;
    });

    Route::run();

} catch (Exception $e) {
    echo $e->getMessage();
}