<?php
/** @var \App\Services\View $this */
?>
<style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 20px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
    }

    .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: auto;
    }
    .form-signin .checkbox {
        font-weight: 400;
    }
    .form-signin .form-control {
        position: relative;
        box-sizing: border-box;
        height: auto;
        padding: 10px;
        font-size: 16px;
    }
    .form-signin .form-control:focus {
        z-index: 2;
    }
    .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }
    .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
</style>
<div class="text-center">
    <form class="form-signin" method="post">
        <h1 class="h3 mb-3 font-weight-normal">Register</h1>

        <label for="inputName" class="sr-only">Nome</label>
        <input type="text" id="inputName" name="name" class="form-control" placeholder="Nome" autofocus>
        <?php $this->includeMsgErrors('name') ?>

        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email" >
        <?php $this->includeMsgErrors('email') ?>

        <label for="inputPassword" class="sr-only">Senha</label>
        <input type="password" id="inputPassword" name="password"  class="form-control" placeholder="Senha" >
        <?php $this->includeMsgErrors('password') ?>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>

        <br><br>
        <a href="/login">Já possuo uma conta</a>
    </form>
</div>
