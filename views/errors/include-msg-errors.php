<?php
/** @var \App\Services\View $this */
$errorsMsgs = $this->getParam('errorsMsgs');
if (!empty($errorsMsgs)):
?>
<div class="alert alert-danger" role="alert">
    <?php
    foreach ($errorsMsgs as $msg):
        echo "$msg<br>";
    endforeach;
    ?>
</div>
<?php
endif;
?>