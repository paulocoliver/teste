<?php
/** @var \App\Services\View $this */
?>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Calculadora</h1>
</div>

<form method="post" style="margin-top: 50px;">
    <div class="form-group row">
        <label for="expression" class="col-sm-2 col-form-label col-form-label-lg">Expression:</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" id="expression" name="expression" aria-describedby="expression" placeholder="Expression"
                   value="<?php echo $_POST['expression']??'10+20+2'?>">
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-8">
            <button type="submit" class="btn btn-primary float-right">Calcular</button>
        </div>
    </div>

</form>

<?php
echo $this->includeMsgErrors('expression');

/** @var \App\Models\MathOperation $mathOperation */
$mathOperation = $this->getParam('mathOperation');
if (!empty($mathOperation)):
?>
<div class="row">
    <div class="alert alert-primary col-sm-8" role="alert">
        Resultado: <?php echo $mathOperation->result ?>
    </div>
</div>
<?php
endif;
?>